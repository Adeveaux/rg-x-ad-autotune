% --- --- --- --- --- --- --- --- --- %
%     Author : Alix DEVEAUX           %
%     Name : FirstIterationQ3.m        %
% Signal analysis, basic compilation  %
% --- --- --- --- --- --- --- --- --- %
clear all;
clc;

%% READING THE FILE
addpath autotune-project-alixarno/Base_Sound/
nom_audiosweep = 'synth_beep_1.wav';
[signalsweep,fssweep] = audioread(nom_audiosweep);%,[debut fin]
n = length(signalsweep);

%% --- --- --- Q1 --- --- --- %%
%% --- TIME ANALYSIS
% --- plot time
figure(1);
plot(signalsweep);

%legend
legend("Guitare signal - sweep1")
xlabel('Time');ylabel('Signal');
grid;
title('TEMP SIGNAL ANALYSIS');


%% FREQ ANALYSIS
h = fft(signalsweep); 
w = [0:n/2-1];

% plot freq
figure(2);
plot(w/2*pi,20*log(abs(h(1:end/2))));

%legend
legend("Half of the Guitare signal (symetrical)");grid ;
xlabel('Freq');ylabel('Magn en dB');
title('FREQ SIGNAL ANALYSIS');

% plot heatmap
%colorbar();

%% Q2 %%
nb_points = fssweep/100 ; % fs/100 <=> points tous les 10 ms
matrice = reshape(signalsweep(1:end-mod(n,nb_points)),nb_points, []);
matriceB = repmat(blackman(nb_points),1,size(matrice,2));


targetFFT = 2048;
sizeOut = targetFFT - size(matrice,1);
matPadded = [matriceB ; zeros(sizeOut,size(matriceB,2))]; % c bo ;'(

matriceApod = fft(matrice .* matriceB);

matriceMod = abs(matriceApod);
matriceAngle = angle(matriceApod);

newMatMod = 20*log(matriceMod(1:end/2,:));

%y = [0 fs/2];
%x = [0 size(matriceMod(),2)/nb_points];
y = [0 fssweep/2];
x = [0 (fssweep/(size(newMatMod(),2)))/2]; % on en est là (ça marche pas évidemment)
% On cherche à faire la même figure que la 3.1 et on arrive pas à faire le
% time
imagesc(x,y,newMatMod); 

set(gca,'Ydir','normal')
xlabel('Time [s]');ylabel('Frequency Hz')
colorbar