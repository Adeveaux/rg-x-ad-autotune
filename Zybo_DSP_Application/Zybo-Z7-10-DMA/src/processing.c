// Processing functions
#include "processing.h"
#include "fixFFT/fixFFT.h"
#include <stdint.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>


int16_t gamme[84] = {16, 17, 18, 19, 20, 21, 23, 24, 25, 27, 29, 30, 32, 34, 36, 38,41,43,
                     46,49,51,55,58,61,65,69,73,77,82,87,92,98,103,110,116,
                     123,130,138,146,155,164,174,185,196,207,220,233,246,261,
                     277,293,311,329,349,369,392,415,440,466,493,523,554,587,
                     622,659,698,739,783,830,880,932,987,1046,1108,1174,1244,
                     1318,1396,1479,1567,1661,1760,1864,1975,2093,2217,2349,
                     2489,2637,2793,2958,3135,3322,3520,3729,3951,4186,4434,
                     4698,4978,5274,5587,5919,6271,6644,7040,7458,7902 };


int16_t frequencySampling = 8000;
int16_t frequencySamplingFP = 32000; // Q(16,13,2)


void benchmark(int16_t* sig_out,int16_t* sig,int64_t size){
   //TODO Iteration 4: You will use this function, but nothing has to be modified in the function
    int mc_runs = 1;
    struct_timer timer;
    /*--------------------------------------------------------------------------
     * --- Main benchmark call (Monte carlo)
     * --------------------------------------------------------------------------*/
    printf("Starting routine\n");
    timer = tic();
    for (int c = 0 ; c < mc_runs ; c++){
        processing(sig_out,sig,size);
    }
    timer = toc(timer);
    printf("output\n");
    print_toc(timer);
}


int processing(int16_t* sig_out,int16_t* sig, int64_t size)
{
   //TODO Iteration 2: This should fill the function to synthetize sig_out, a new signal (independant from sig)
    // 1 equivaut à pi/2 et 2 équivant à pi
    int16_t x = 0;
    int16_t pi = (int16_t)(1 * (1 << 14)); // pi/2 en Q(16,2,13)
    for (int64_t i = 0; i < size; i++){
        int64_t x = i * pi / size; // calcul en utilisant des entiers
        int16_t xdecal = (int16_t)(x); // pas besoin de décaler x, car il est déjà en Q(16,2,13)
        int16_t valeurFPSIN = fpsin(xdecal);
        sig_out[i] = valeurFPSIN;
        printf("Valeur de fpsin: %d\n", valeurFPSIN);
        }
    return 0;
}
   //TODO Iteration 3: This should fill the function to synthetize a sig_out, new signal based from sig 
   //TODO Iteration 4: This should update this function to synthetize a signal sig_out, with part of sig


int16_t absolute(int16_t x){
	if (x<0){
		return -x;
	}
	else{
		return x;
	}
}


/* We use halfword (Int16_t) and with classic product it takes LSB and not MSB 
 * => Ensure double precision multiplier and shift to take MSB 
 */
inline int32_t mul_int16_int16(int16_t x,int16_t y)
{
    return  ((int32_t)x *  (int32_t)y);
}


//   Inputs: 
//       valeur : nb sur lequel on va réaliser le décalage
//       decalage : valeur du decalage a realiser 
//
//   Output
//       result : nombre decaler et remis sur 16 bits

int16_t rshift(int32_t valeur, int decalage) {
    if (decalage >= 0) {
        return (int16_t) (valeur >> decalage);
    } else {
        // Si shift est négatif, il s'agit d'un décalage vers la gauche.
        return (int16_t) (valeur << -decalage);
    }
}

int16_t absolute(int16_t x){
	if (x>0){
		return x;
	}
	else{
		return -x;
	}
}


//   Inputs: 
//       x: input data en 16 bits
//
//   Output
//       result : sinus approché de x en 16 bits


int16_t fpsin(int16_t x) // entrée en Q(16,2,13)
{
   //TODO: Iteration 2: Let's implement a sine in Fixed Point domain ! 
    
    // on ramene x dans l'interval [-1;1] comme au début
    if (x > 8192){
        x = 16384-x;
    }
    else if (x < - 8192){
        x = -16384-x;
    }

    bool negatif = (x<0);
    x = absolute(x);


    /* =====================================================
       ============= Calculs des coeff a,b,c =============== 
       ===================================================== */

    int16_t a = 25718; // a en fixed point
	int16_t b = -20953; // b en fixed point Q(16, 0, 15)
	int16_t c = 18276; // c en fixed point Q(16, -3, 18)


    /* ==================================================
       ============== Calculs des x_barre =============== 
       ================================================== */


    /* ============ x² ============ */
    int16_t x_carre = rshift(mul_int16_int16(x, x),13); //Q(16,0,15)
    
    /* ============ x³ ============ */
    int16_t x_cube = rshift(mul_int16_int16(x, x_carre),13); //Q(16,0,15)

    /* ============ x⁵ ============ */
    int16_t x_cinq = rshift(mul_int16_int16(x_carre, x_cube),13); //Q(16,0,15)


    /* ===========================================================
       ============ Calculs des coefficients du sinus ============ 
       =========================================================== */

    /* ============ a * x ============ */
    int32_t a_x = mul_int16_int16(a, x) << 2;

    /* ============ b * x³ ============ */
    int32_t b_x_cube = mul_int16_int16(b, x_cube) << 1;

    /* ============ c * x_barre⁵ ============ */
    int32_t c_x_cinq = mul_int16_int16(c, x_cinq) >> 2;


    /* ========================================
       ============ Calculs finaux ============ 
       ======================================== */

    int32_t somme_tempo = a_x + b_x_cube;
    int32_t result_32 = somme_tempo + c_x_cinq;
    int16_t result = rshift(result_32, 14);
   
    if (negatif){
        result = -result;
    }
    return result;
}

/* fpsin prend en entrée un type bien particulier, on va creer une fonction eviter les pb*/

// On va calculer w = 4*F_c/F_s 
int16_t w_pour_fpsin(int16_t F_c) { // sin_freq is coded in Q(16,12,3)

	int16_t T_s = 16777; // Dans notre code Fs = 8000 donc d'après Matlab Q(16,-12,27)
	int16_t w = 0;

    // calcul de la partie F_c/F_s
	w = (mul_int16_int16(F_c,T_s)) >> 16; // sortie du multiplieur en Q(32,1,30) passage en Q(16,1,14)

    int16_t quatre_en_16 = 16384;  

    // calcul final de w = 4*F_c/F_s
	w = (mul_int16_int16(w,quatre_en_16)) >> 13; // sortie du multiplieur en Q(32,5,26) et passage Q(16,2,13);
	
    return w; // format en Q(16,2,13) celui attendu pour l'entrée en fpsin
}

/* Manipulation de pointeurs pour éviter de manipuler des tableaux */

//   Inputs: 
//       sig_in : pointeur qui va contenir les valeurs sur lesquelles on applique un traitement
//       phi : phase du signal sur lesquelles on applique un traitement
//       size : taille du signal sur lesquels on applique un traitement
//
//
//   Output
//       Aucun : On manipule des pointeurs, il n'y a donc aucune sortie

void pitch_synthesis(int16_t* sig_in, int16_t* phi, int64_t size){

    
    int16_t T_s = 16777; // Dans notre code Fs = 8000 donc d'après Matlab Q(16,-12,27)
    //duree = size * T_s = 40000 * 1/8000 = 5
    int16_t duree = 20481; // en mettant sur Q(16,3,12) (format d'entrée de fpsin)

    //on veut la fréquence max de la fft donc on utilise la fonction code a cet effet
    int16_t frequence_max = pitch_detection(sig_in, size);

    int16_t frequenceGamme = freqGamme(frequence_max);
    int16_t frequenceGamme_FP = frequenceGamme << 3 ; // mise au format Q(16,12,3)

    /* pour utiliser fpsin, on va passer la fréquence dans w_pour_fpsin */
	int16_t w = w_pour_fpsin(frequenceGamme_FP); //format Q(16,1,14)
	int16_t w_n[size]; 

    /* On veut un signal de la forme sin( w + phi) comme vu durant le TP */

    int16_t phi_transition = (mul_int16_int16(frequenceGamme_FP,T_s) >> 15); // (Q(32,1,30) mise au format Q(16,0,15) le bit de partie entière n'est pas utile
    phi_transition = (mul_int16_int16(duree, phi_transition) >> 12); // sortie du multiplieur en Q(32,4,27) mise au format Q(16,0,15)
	int16_t quatre_en_16 = 16384; //Q(16,3,12)
    phi_transition = (mul_int16_int16(phi_transition, quatre_en_16)>>14); // sortie Q(32,4,27) puis mise au format pour entree dde fpsin Q(16.2.13)


    /* w_n doit augmenter à chaque tour donc on lui ajoute w à chaque tour */

    w_n[0] = 0; // a l'etat intial pulsation a 0
    for (int64_t i = 1; i < size; i++ ){
        w_n[i] = w_n[i - 1] + w; //quand on parcourt la liste w_n, on augmente de w a chaque case
    }

    /* on a maintenant tout a notre disposition pour calculer sig_in*/
    for(int64_t i=0; i<size; i++){
		sig_in[i] = fpsin(w_n[i]+ *phi);
    }

    *phi = *phi + phi_transition; // mise à jour de phi pour l'étape suivante format entree fpsin Q(16,2,13)
}

/* Comme demande, on va utiliser une fonction pitch_detection */

//   Inputs: 
//       signal[] : tableau qui va contenir les valeurs sur lesquelles on applique la fft
//       size : taille du signal sur lesquels on applique un traitement
//
//   Output
//       frequence : la frequence max pour la valeur absolue de la fft

int16_t pitch_detection(int16_t sig[], int64_t size){


    int16_t frequence; //initialisation de notre valeur de sortie

	int16_t fft_reelle[SIZE_FFT]; //partie de la FFT de la partie reelle du signal
	int16_t fft_imaginaire[SIZE_FFT]; //partie de la FFT de la partie imaginaire du signal
	int16_t fft_module[SIZE_FFT]; // FFT du signal après passage au module
    /* fft_imaginaire n'est pas utile car on traite un signal reel donc aucune partie imaginaire mais
    nous allons nous en servir dans la fonction qui nous est donnee calculant la fft*/

	int16_t module_fft_reel;
    int16_t module_fft_imaginaire;

    for(int64_t i = 0 ; i < size; i++){
		fft_reelle[i] = sig[i]; //on rentre dans fft_reelle les valeurs du signal qu'on veut traiter
        fft_imaginaire[i] = 0; //aucune partie imaginaire
	}

    for(int64_t i = size ; i < SIZE_FFT; i++){
        /* Si le signal comporte moins de donnees que celle attendu pour realiser une FFT, 
        on ajoute manuellement des 0 qui n'influeront pas l'information contenue par le signal d'origine*/
		fft_reelle[i] = 0; 
        fft_imaginaire[i] = 0;
	}

    //les signaux sont prets pour realiser la fft
    int retour_fft = fix_fft(fft_reelle, fft_imaginaire, POWER_FFT, 0); //on utilise la fonction fournie 
    //cette fonction modifie directement dans les fichiers fft_reelle et fft_imaginaire

    //on va maintenant calculer la valeur absolue de la fft
    for (int64_t i = 0; i < SIZE_FFT; i++){
        module_fft_reel = (mul_int16_int16(fft_reelle[i],fft_reelle[i])>>16); // sortie du multiplieur en Q(32,1,30) passage en Q(16,1,14)
        module_fft_imaginaire = (mul_int16_int16(fft_imaginaire[i],fft_imaginaire[i])>>16); // sortie du multiplieur en Q(32,1,30)
        fft_module[i] = module_fft_reel + module_fft_imaginaire;
    }

    int indice_du_max = indice_max(fft_module,size); //jusqu'a size car le reste on a que des 0

    frequence = (mul_int16_int16(indice_du_max, frequencySamplingFP))>>13; // on se ramene a Q(16,2,13)
    
	return frequence;
}


/* On cherhce a obtenir l'indice de la frequence de la fft la plus elevee */

//   Inputs: 
//       tab[] : tableau contenant les valeurs de la fft en module
//       size : taille du signal sur lesquels on applique un traitement
//
//   Output
//       indice : indice de la frequence max pour la valeur absolue de la fft

int indice_max(int16_t tab[], int64_t size){
	int16_t m = absolute(tab[0]);
	int indice = 0;
	for (int64_t i = 1; i < size; i++){
		if(m < absolute(tab[i])){
			m = absolute(tab[i]);
			indice = i;
		}
	}//on a parcouru tout le tableau, on tient donc l'indice du max 
    return indice;
}


/* On cherhce a obtenir la frequence de la gamme la plus proche
 de la frequence de la fft en module la plus elevee */

//   Inputs: 
//       frequence_max : tableau contenant les valeurs de la fft en module
//       size : taille du signal sur lesquels on applique un traitement
//
//   Output
//       frequence_approche : frequence frequence de la gamme la plus proche de la max de la fft

int16_t freqGamme(int16_t frequence_max){
    int16_t frequence_approche;
	int16_t distance_min = absolute(frequence_max - gamme[0]); // variable qui contient la distance minimal entre la fr
	int16_t indice = 0;
	int16_t taille = 84; //taille du tableau contenant les frequences de la gamme

	for(int64_t i = 1; i < taille; i++){

		int16_t distance = absolute( frequence_max - gamme[i]);
		if(distance_min > distance){
			distance_min = distance;
			indice = i;
		}
	}
    frequence_approche = gamme[indice];
	return frequence_approche;
}

/* On va decouper en bloc le signal et travailler sur la frequence max de chaque decoupage */

//   Inputs: 
//       sig : pointeur qui contient les valeurs sur lesquelles on applique un traitement
//       sig_out : pointeur qui va contenir les valeurs de sig sur lesquelles on applique un traitement
//       size : taille du signal sur lesquels on applique un traitement
//
//
//   Output
//       Aucun : On manipule des pointeurs, il n'y a donc aucune sortie

void pitch_detection_blocks(int16_t* sig, int16_t* sig_out, int64_t size){

    int16_t phi = 0; 		// initialisation de la phase à 0
	int16_t nb_valeur_bloc = 400; 	// on veut 4000 valeur par bloc
	int16_t nb_bloc = 100; // size/nb_valeur_bloc = 40000/400 = 100

    int16_t bloc_transition[nb_valeur_bloc]; //bloc qui va contenir nos valeurs de transition pour le traitement

    for (int64_t i = 0; i < nb_bloc; i++ ){
        /* on va apppliquer appliquer le même traitement sur les 100 blocs */
        int64_t decal_bloc = i * nb_valeur_bloc;
        for (int64_t j = decal_bloc; j < (i+1) * nb_valeur_bloc; j++ ){
            /* avec i * nb_valeur_bloc, on va remplier par tronçons de 400 valeurs */
            bloc_transition[j - decal_bloc] = sig[j];
        }
        // application du traitement sur la partion de bloc selectionnee
        pitch_synthesis(bloc_transition, &phi, nb_valeur_bloc);

        for(int64_t k = decal_bloc; k<(i+1)*nb_valeur_bloc; k++){
			sig_out[k] = bloc_transition[k-decal_bloc];
		}
    }
}
