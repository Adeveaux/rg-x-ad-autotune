connect -url tcp:127.0.0.1:3121
source /users/elo/adeveaux/Bureau/rg-x-ad-autotune/Zybo_DSP_Application/system_wrapper_hw_platform_0/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Zybo Z7 210351AB726DA"} -index 0
loadhw -hw /users/elo/adeveaux/Bureau/rg-x-ad-autotune/Zybo_DSP_Application/system_wrapper_hw_platform_0/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent Zybo Z7 210351AB726DA"} -index 0
stop
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Digilent Zybo Z7 210351AB726DA"} -index 0
rst -processor
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Digilent Zybo Z7 210351AB726DA"} -index 0
dow /users/elo/adeveaux/Bureau/rg-x-ad-autotune/Zybo_DSP_Application/Zybo-Z7-10-DMA/Debug/Zybo-Z7-10-DMA.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Digilent Zybo Z7 210351AB726DA"} -index 0
con
