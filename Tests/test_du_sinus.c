#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>
#include <CUnit/Basic.h>
#include "test_du_sinus.h"


/* We use halfword (Int16_t) and with classic product it takes LSB and not MSB 
 * => Ensure double precision multiplier and shift to take MSB 
 */
inline int32_t mul_int16_int16(int16_t x,int16_t y)
{
    return  ((int32_t)x * (int32_t)y);
}


//   Inputs: 
//       valeur : nb sur lequel on va réaliser le décalage
//       decalage : valeur du decalage a realiser 
//
//   Output
//       result : nombre decaler et remis sur 16 bits

int16_t rshift(int32_t valeur, int decalage) {
    if (decalage >= 0) {
        return (int16_t) (valeur >> decalage);
    } else {
        // Si shift est négatif, il s'agit d'un décalage vers la gauche.
        return (int16_t) (valeur << -decalage);
    }
}


//   Inputs: 
//       x: input data en 16 bits
//
//   Output
//       result : sinus approché de x en 16 bits
int16_t absolute(int16_t x){
    if (x<0){
        return -x;
    }
    else{
        return x;
    }
}

int16_t fpsin(int16_t x) // entrée en Q(16,2,13)
{
   //TODO: Iteration 2: Let's implement a sine in Fixed Point domain ! 
    
    // on ramene x dans l'interval [-1;1] comme au début
    if (x > 8192){
        x = 16384-x;
    }
    else if (x < - 8192){
        x = -16384-x;
    }
    printf("Valeur de x apres intervalle [-1;1] = [-8192;8192]  : %d\n", x);

    bool negatif = (x<0);
    x = absolute(x);

    printf("Valeur de x apres abs : %d\n", x);

    /* =====================================================
       ============= Calculs des coeff a,b,c =============== 
       ===================================================== */

    int16_t a = 25718; // a en fixed point
	int16_t b = -20953; // b en fixed point Q(16, 0, 15)
	int16_t c = 18276; // c en fixed point Q(16, -3, 18)


    /* ==================================================
       ============== Calculs des x_barre =============== 
       ================================================== */


    /* ============ x² ============ */
    int16_t x_carre = rshift(mul_int16_int16(x, x),13); //Q(16,0,15)
    printf("\t Valeur de x_carre: %d\n", x_carre);
    
    /* ============ x³ ============ */
    int16_t x_cube = rshift(mul_int16_int16(x, x_carre),13); //Q(16,0,15)
    printf("\t Valeur de x_cube: %d\n", x_cube);


    /* ============ x⁵ ============ */
    int16_t x_cinq = rshift(mul_int16_int16(x_carre, x_cube),13); //Q(16,0,15)
    printf("\t Valeur de x_cinq: %d\n", x_cinq);

    /* ===========================================================
       ============ Calculs des coefficients du sinus ============ 
       =========================================================== */

    /* ============ a * x ============ */
    int32_t a_x = mul_int16_int16(a, x) << 2;

    /* ============ b * x³ ============ */
    int32_t b_x_cube = mul_int16_int16(b, x_cube) << 1;

    /* ============ c * x_barre⁵ ============ */
    int32_t c_x_cinq = mul_int16_int16(c, x_cinq) >> 2;


    /* ========================================
       ============ Calculs finaux ============ 
       ======================================== */

    int32_t somme_tempo = a_x + b_x_cube;
    int32_t result_32 = somme_tempo + c_x_cinq;
    int16_t result = rshift(result_32, 14);
   

    if (negatif){
        result = -result;
    }
    printf("%f\n",result * pow(2,-15)); // on s'est ramené à un résultat en 16 bits
    return result;
}


int main() {
    int64_t i = 0;
    int64_t size = 100;
    int16_t pi = (int16_t)(1 * (1 << 14)); // pi/2 en Q(16,2,13)
    for (i = 0; i < size; i++){
        int64_t x = i * pi / size; // calcul en utilisant des entiers
        int16_t xdecal = (int16_t)(x); // pas besoin de décaler x, car il est déjà en Q(16,2,13)
        int16_t valeurFPSIN = fpsin(xdecal);
        printf("Valeur de fpsin: %d\n", valeurFPSIN);
    }
    
    return 0;
}
