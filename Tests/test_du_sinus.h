#include <stdio.h>
#include <math.h>

/* Mathematical macros */
#define POWER_FFT 10
#define SIZE_FFT (1<<POWER_FFT)   // FIXME Assert SIZE_FFT = 2^POWER_FFT
#define M_PI   3.14159265358979323846

/*  =======================================
    ============== Sinus FP =============== 
    ======================================= */
int16_t fpsin(int16_t x);
int16_t rshift(int32_t valeur, int decalage); 
int32_t mul_int16_int16(int16_t x,int16_t y); // on utilise un mult16 mais son retour sera en 32 bits.