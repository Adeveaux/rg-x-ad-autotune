#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>
#include "test_du_sinus.h"


/* We use halfword (Int16_t) and with classic product it takes LSB and not MSB 
 * => Ensure double precision multiplier and shift to take MSB 
 */
inline int32_t mul_int16_int16(int16_t x,int16_t y)
{
    return ((int32_t) x) * ((int32_t) y);
}


//   Inputs: 
//       x: input data en 16 bits
//
//   Output
//       result : sinus approché de x en 16 bits


int16_t fpsin(int16_t x)
{
   //TODO: Iteration 2: Let's implement a sine in Fixed Point domain ! 
    
    // on ramene x dans l'interval [-2;2] comme au début
    x =  2 - (x % 4); // pour cette question, on utilise le modulo 4

    // on ramene x dans l'interval [-1;1] comme au début
    if (x > 1){
        x = 2-x;
    }
    else if (x < -1){
    x = -2-x;
    }

    bool negatif = (x<0);
    x = abs(x);

    /* =====================================================
       ============= Calculs des coeff a,b,c =============== 
       ===================================================== */

    //coeff a
    float a = (12/M_PI) - 9/4;
    // coeff b
    float b = -2 * a + 5 / 2;
    int Nb_bits_b = 15 - NbBits(b);
    int16_t b_entier = FixCode(b, Nb_bits_b) * pow(2, Nb_bits_b); // nb sur 16 bits Q(16, 0, 15)
    int16_t a_entier = (int16_t)a; // nb sur 16 bits Q(16, 0, 15)
    printf("\t Valeur de a: %f \n", a);
    printf("\t Valeur de a_ent: %d\n", a_entier);
 
 
    // coeff b
    float b = -2 * a + 5 / 2;
    int Nb_bits_b = 15 - NbBits(b);
    int16_t b_entier = FixCode(b, Nb_bits_b) * pow(2, Nb_bits_b); // nb sur 16 bits Q(16, 0, 15)

    // coeff c
    float c = a - 3 / 2;
    int Nb_bits_c = 15 - NbBits(c);
    int16_t c_entier = FixCode(c, Nb_bits_c) * pow(2, Nb_bits_c);
    // nb sur 16 bits Q(16, -3, 18)


    /* ==================================================
       ============== Calculs des x_barre =============== 
       ================================================== */
    int16_t x_barre = x;
    int Nb_bits_x_barre = 15;
    /* nb sur 16 bits Q(16, 0, 15) */
    int16_t x_barre_entier = FixCode(x_barre, Nb_bits_x_barre) * pow(2,Nb_bits_x_barre);
    printf("\t Valeur de x_barre: %d\n", x_barre);
    printf("\t Valeur de x_barre_carre_entier: %d\n", x_barre_entier);

    /* ============ x_barre² ============ */
    // sortie mult Q(32, 1, 30)
    int32_t x_barre_carre_entier32 = mul_int16_int16(x_barre_entier, x_barre_entier);
    // nb sur 16 bits Q(16, 0, 15) on repasse en 16 bits
    int16_t x_barre_carre_entier = rshift(x_barre_carre_entier32, 15);
    printf("\t Valeur de x_barre_carre_entier32: %d\n", x_barre_carre_entier32);
    printf("\t Valeur de x_barre_carre_entier: %d\n", x_barre_carre_entier);
    
    /* ============ x_barre³ ============ */
    // sortie mult Q(32, 1, 30)
    int32_t x_barre_cube_entier32 = mul_int16_int16(x_barre_entier, x_barre_carre_entier);
    // nb sur 16 bits Q(16, 0, 15)
    int16_t x_barre_cube_entier = rshift(x_barre_cube_entier32, 15);
     
    /* ============ x_barre⁵ ============ */
    // sortie mult Q(32, 1, 30)
    int32_t x_barre_cinq_entier32 = mul_int16_int16(x_barre_cube_entier, x_barre_carre_entier);
    // nb sur 16 bits Q(16, 0, 15)
    int16_t x_barre_cinq_entier = rshift(x_barre_cinq_entier32, 15);


    /* ===========================================================
       ============ Calculs des coefficients du sinus ============ 
       =========================================================== */

    /* ============ a * x_barre ============ */
    // sortie mult Q(32, 2, 29)
    int32_t a_x_barre_entier32 = mul_int16_int16(a_entier, x_barre_entier);

    /* ============ b * x_barre³ ============ */
    // sortie mult Q(32, 1, 30)
    int32_t b_x_barre_cube_entier32 = mul_int16_int16(b_entier, x_barre_cube_entier);
    // sortie Q(32, 2, 29)
    b_x_barre_cube_entier32 = rshift_32(b_x_barre_cube_entier32, 1); 
    

    /* ============ c * x_barre⁵ ============ */
    // sortie mult Q(32, -2, 33)
    int32_t c_x_barre_cinq_entier32 = mul_int16_int16(c_entier, x_barre_cinq_entier);
    // sortie Q(32, 2, 29)
    c_x_barre_cinq_entier32 = rshift_32(c_x_barre_cinq_entier32, 4);

    /* ========================================
       ============ Calculs finaux ============ 
       ======================================== */

    int32_t somme_tempo = add_int32_int32(a_x_barre_entier32, b_x_barre_cube_entier32);
    int32_t result_32 = add_int32_int32(somme_tempo, c_x_barre_cinq_entier32);
    //int16_t result =result_32/32;
    int16_t result = rshift(result_32, 14);
    result = result * pow(2,-15); // on s'est ramené à un résultat en 16 bits

    /*printf("\t Valeur de result_32: %ld\n", result_32);
    printf("\t Valeur de result (avant conversion): %d\n", result);
    printf("\t Valeur de result: %d\n", result);*/

    if (negatif){
        result = -result;
    }
    return result;
}


//   Inputs: 
//       x: input data 
//       N: number of bits for the fractional part
//
//   Output
//       y : donne la puissance de 2 la plus proche de x 

int FixCode(int16_t x, int N) {
    return (int)round(x * pow(2, N)) * pow(2, -N);
}


//   Inputs: 
// 	%       x: data dynamic 
// 	%
// 	%   Output
// 	%       mx : number of bits required to code x

// 	% --- Return number of bits required to code the integer part 
// 	% of entry x 
// 	% --- Considering that data can be atmost on 32bits 


int NbBits(double x) {
    // Return number of bits required to code the integer part of x
    double absX = fabs(x);
    int mx = 1 + floor(log2(absX));

    // If mx is -Inf, set mx to 0
    if (mx == -INFINITY) {
        mx = 0;
    }

    return mx;
}

int main() {

    // Calcul du signe
    bool negatif = (-10 < 0);
    float x = 1.;
    int Nb_bits_x = 15 - NbBits(x); // 15 car 16 bits de dispo et 1 pour le signe donc 16 - 1 
    // Affichage de la valeur de negatif
    printf("Valeur de x_entier : %d\n", Nb_bits_x);
    int16_t x_entier = FixCode(x,Nb_bits_x) * pow(2, Nb_bits_x); // nb sur 16 bits Q(16, 0, 15)
    // Calcul du sinus en virgule fixe
    int16_t fpsinValue = fpsin(x_entier);
    // Affichage de la valeur de negatif
    printf("Valeur de x_entier : %d\n", x_entier);
    // Affichage de la valeur de negatif
    printf("Valeur de negatif : %d\n", negatif);

    // Calcul et affichage de 10 % 4
    //int resultat = 10 % 4;
    //printf("Le résultat de 10 %% 4 est : %d\n", resultat);
    
     // Boucle de 1 à 10
    /* for (int angle = 1; angle <= 10; ++angle) {
        // Calcul du sinus en virgule flottante
        double angle_radians = angle * M_PI / 180.0;
        double sinValue = sin(angle_radians);

        // Calcul du sinus en virgule fixe
        int16_t fpsinValue = fpsin((int16_t)angle);

        // Calcul du sinus en virgule fixe (conversion)
        int16_t sinFixed = (int16_t)(sinValue * (1 << 15));

        // Affichage des résultats
        printf("Angle : %d\n", angle);
        printf("Sinus (double) : %f\n", sinValue);
        printf("Sinus (fixe) : %d\n", fpsinValue);
        printf("Sinus (fixe, conversion) : %d\n", sinFixed);
        printf("\n");
    }
 */
    return 0;
}