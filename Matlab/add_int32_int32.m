function tmp = add_int32_int32(a,b)
    % Bounds for the given type 
    vMax = lshift(1,31)-1;
    vMin = -lshift(1,31);
    % Pure sum, not typed 
    tmp = a + b ;
    % Check values 
    if tmp > vMax 
        tmp = vMin + (tmp - vMax);
    elseif tmp < vMin 
        tmp = vMax - (-tmp + vMin); 
    end
    tmp = double(tmp);
end