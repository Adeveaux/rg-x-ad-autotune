% close;
pas = pow2(-10);
t =-20:pas:20;
taille = length(t);


sin_aprox = zeros(taille,1);
i = 1;

for x = t
    sin_aprox(i) = function_sin_approch(x);
    i= i+1;
end

sin_matlab = sin(t*pi/2);

figure
plot(t, sin_matlab, t, sin_aprox);
title("Sinus obtenu avec approximation et avec la formule de Matlab");
grid;

mse = 10 * log10(mean(abs(sin_aprox - sin_matlab.').^2))