function [s,phiOut] = pitch_synthesis(sig, fe, phi)
    
    N = length(sig);

    duree = N/fe;
    pas = duree/(N-1);
    t = [0:pas:duree];

    FFT = fft(sig);
    M = length(FFT);
    FFTprime = FFT(1:M/2);
    
    [~,ind] = max(abs(FFTprime));
    freq = max(fe/2 * (ind-1) / M);

    frequenceGamme = freqGamme(freq);

    s = sin(2*pi*frequenceGamme(1)*t);
    phiOut = phi + 2*pi*frequenceGamme*(duree+pas);

end


function f = freqGamme (f) % RENVOI LA FREQ LA PLUS PROCHE DANS LA GAMME

    freqCMinor = [16.35, 32.70, 65.41, 130.81, 261.63,277.18,293.66,311.13,329.63,349.23,369.99,392.00,415.30,440.00,466.16,493.88,523.25,554.37,587.33,622.25,659.25,698.46,739.99,783.99,830.61,880.00,932.33, 987.77,1046.50,1108.73,1174.66,1244.51,1318.51,1396.91,1479.98,1567.98,1661.22,1760.00,1864.66,1975.53,2093.00,2217.46,2349.32,2489.02,2637.02,2793.83,2958.96,3135.96,3322.44,3520.00,3729.31,3951.07,4186.01,4434.92,4698.63,4978.03,5274.04,5587.65,5919.91,6271.93,6644.88,7040.00,7458.62,7902.13];
    j = 1;
    
    while (f > freqCMinor(j) && j < length(freqCMinor))
        j = (j+1);
    end
    f = freqCMinor(j);
end
