function    mx = NbBits(x)

	%   Inputs: 
	%       x: data dynamic 
	%
	%   Output
	%       mx : number of bits required to code x

	% --- Return number of bits required to code the integer part 
	% of entry x 
	% --- Considering that data can be atmost on 32bits 
	mx = 1+floor(log2(max(abs(x(:)))));
    if mx == -Inf
        mx=0;
    end
end
