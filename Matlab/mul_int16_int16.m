function res = mul_int16_int16(a,b)
res = int32(int32(a) * int32(b));
end