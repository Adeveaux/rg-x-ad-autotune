function signal = signal_reconstruction(Fs)
   Tt = 5; % durée totale du signal en s
   k = Tt * Fs;
   nb_echan = Tt/(k-1); 

   t1 = [0 : nb_echan: 0.05];
   t2 = [0.05 : nb_echan: 0.45];
   t3 = [0.32: nb_echan: 3];
   t4 = [2.88 : nb_echan: 4.03 ];
   t5 = [3.90 : nb_echan: 5 ];

   val1 = 30;
   val2 = 350;
   val3 = 2500;
   val4 = 1200;
   val5 = 400;

   ss1 = sin(2*pi*t1*val1);
   ss2 = sin(2*pi*t2*val2);
   ss3 = sin(2*pi*t3*val3);
   ss4 = sin(2*pi*t4*val4);
   ss5 = sin(2*pi*t5*val5);

   signal = [ss1 ss2 ss3 ss4 ss5];

end