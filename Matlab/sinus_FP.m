%function vecteur = sinus_FP(Fc, Fs, N)
    
    N=128;
    Fc = 1024;
    Fs = 8000;
    vecteur = zeros(N,1);
    sin_matlab1 = zeros(N,1);
    a  = 1/Fs; %on a une valeur <1
    Nb_bits_a = 15; % on à les 15 bits pour la partie fractionnaire
    a_entier = 4*FixCode(a,Nb_bits_a)*pow2(Nb_bits_a); % nb sur 16 bits Q(16,0,15)
    
    Nb_bits_Fc = 3; % 4000 Hz max donc log2(4000)=12 donc 3 bits pour partie frac
    Fc_fp = FixCode(Fc,Nb_bits_Fc)*pow2(Nb_bits_Fc); % nb sur 16 bits  Q(16,12,3)

    w_32 = mul_int16_int16(a_entier,Fc_fp); %sortie mult Q(32,13,18)
    w = rshift(w_32, 4); % nb sur 16 bits Q(16,1,14)
    
    w_n = 0;

    for i = 1: N 
        w_n = add_int16_int16(w_n, w);
        vecteur(i) = function_sin_approch(w_n*pow2(-14));
        sin_matlab1(i) = sin(w_n*pow2(-14));
    end

    t =0:1:N-1;
    w_n_matlab = 2*pi*Fc/Fs * t;
    sin_matlab2 = sin(w_n_matlab);
    figure
    plot(t, sin_matlab2, t, vecteur, t, sin_matlab1);
    title("Sinus obtenu avec approximation et avec la formule de Matlab");
    xlabel("Nombre de points équivalent à un temps")
    ylabel
    grid;

%end 

