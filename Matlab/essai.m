% répertoire contenant les fichiers .wav
dirName = 'C:\path\to\directory\containing\wav\files';

% répertorier les fichiers .wav
fileList = dir(fullfile(dirName, '*.wav'));

% initialiser un tableau pour stocker les fichiers .wav
numFiles = length(fileList);
wavArray = cell(1, numFiles);

% charger chaque fichier .wav dans un élément de tableau
for i = 1:numFiles
    wavArray{i} = wavread(fullfile(dirName, fileList(i).name));
end