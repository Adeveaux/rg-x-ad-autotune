% --- --- --- --- --- --- --- --- --- %
%     Author : Alix DEVEAUX           %
%     Name : FirstIterationQ3.m        %
% Signal analysis, basic compilation  %
% --- --- --- --- --- --- --- --- --- %
clear all;
clc;

%% READING THE FILE
% addpath ../
% % répertoire contenant les fichiers .wav
% dirName = 'Base_Sound';
% 
% % répertorier les fichiers .wav
% fileList = dir(fullfile(dirName, '*.wav'));
% %fileList = fileList';
% % initialiser un tableau pour stocker les fichiers .wav
% numFiles = length(fileList);
% sigArray = zeros*(1 : numFiles);
% freqArray = zeros*(1 : numFiles);
% sizeArray = zeros*(1 : numFiles); % taille de sigArray
% nameArray = char(zeros*(1 : numFiles));
% hArray = zeros*(1 : numFiles);
% wArray = zeros*(1 : numFiles);
% 
% % charger chaque fichier .wav dans un élément de tableau
% for i = 1:numFiles
%     nameArray(1,i) = fileList(i).name;
% end
% 
% %% --- --- --- Q1 --- --- --- %%
% %% --- TIME ANALYSIS
% % --- plot time
% compteur = 1;
% figure(1)
% for i = 1:4:numFiles
%    
%     subplot(3,1,compteur);
%     plot(sigArray{i});
%     legend(nameArray{i});
%     xlabel('Time');ylabel('Signal');
%     grid;
%     title('TEMP SIGNAL ANALYSIS');
%     compteur=1+compteur;
% end
%  
% %% FREQ ANALYSIS
% for i = 1:numFiles
%     hArray{i} = fft(sigArray{i});
%     wArray{i} = (0:(sizeArray{i}/2-1));
% end
%  
% % plot freq
% compteur2=1;
% figure(2)
% for i = 1:4:numFiles
%     subplot(3,1,compteur2);
%     plot(wArray{i}/2*pi,20*log(abs(hArray{i}(1:end/2))));
%     legend(nameArray{i});
%     xlabel('Freq');ylabel('Magn en dB');
%     grid;
%     title('FREQ SIGNAL ANALYSIS');
%     compteur2 = compteur2 +1;
% end

%% Q2
[sig3, fs3]= audioread("synth_sweep_1.wav");

tracer(sig3, fs3);

%% Q3

[sig_b1, fs_1]= audioread("synth_beep_1.wav");
[sig_b2, fs_2]= audioread("synth_beep_2.wav");


tracer(sig_b1,fs_1);
tracer(sig_b2,fs_2);
Max_Freq_B1 = getMAxFreq(sig_b1, fs_1);
Max_Freq_B2 = getMAxFreq(sig_b2, fs_2);
    
str = sprintf('La fréquence active pour le beep1 est : %d Hz \n',Max_Freq_B1);
disp(str)
str2 = sprintf('La fréquence active pour le beep2 est : %f Hz \n',Max_Freq_B2);
disp(str2)


%%fonctions

function [MAX] = getMAxFreq(sig, fs)
    FFT=fft(sig);
    N = size(FFT,1); % Nb de Ts dans l'échantillon
    [~,ind] =  max(abs(FFT));
    MAX =(ind-1) *(fs/N);
    
end






function tracer(sig, fs)
figure
%[sig, fs]= audioread("guitar_1.wav");
Ts = 1/fs; %temps d'échantillonnage
N = size(sig,1); % Nb de Ts dans l'échantillon
Tt = N * Ts; % Temps total en seconde
Tv_s = 0.2;
Tv = Tv_s*1e3; % on veut des tas de 100ms
approxTv = 2^nextpow2(Tv); % Tv en puissance de 2 approché et en s
k = fix((Tt*10e3)/approxTv); %nb de découpage
p = fix(approxTv/(Ts*10e3)); %nb de point par découpage

tableau_decoupage = reshape(sig(1:k*p),p,k);

matrice = repmat(hamming(p),1,size(tableau_decoupage,2));

matriceApod = fft(tableau_decoupage .* matrice);

matriceMod = abs(matriceApod);

newMatMod = 20*log(matriceMod(1:end/2,:));

y = [0 fs/2];
x = [0 Tt]; % on en est là (ça marche pas évidemment)
% On cherche à faire la même figure que la 3.1 et on arrive pas à faire le
% time
imagesc(x,y,newMatMod);

set(gca,'Ydir','normal')
xlabel('Time [s]');ylabel('Frequency Hz')
colorbar


end