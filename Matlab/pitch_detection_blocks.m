function out = pitch_detection_blocks(sig, Fe, Nblock)

    L = length(sig);
    distBloc = round(L/Nblock);
    phi = 0;
    out = [];

    for k=1:Nblock-1
        %phi = out(end)
        [s,phi] = pitch_synthesis(sig((k-1)*distBloc + (1:distBloc)), Fe, phi);
        phi= angle(s(end));
        out = [out s];
    end

end