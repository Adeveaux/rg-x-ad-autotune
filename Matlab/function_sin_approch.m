
function result = function_sin_approch(x)
%% on ramene x dans l'interval [-2;2] comme au début
x =  2-mod(x,4);

%% on ramene x dans l'interval [-1;1] comme au début
    
if x > 1
   x = 2-x;
elseif x < -1
   x = -2-x;
end 


negatif = (x<0);
x = abs(x);


%coeff a
a = (12/pi) - 9/4;
Nb_bits_a = 15 - NbBits(a); % 15 car 16 bits de dispo et 1 pour le signe donc 16 - 1 
a_entier = FixCode(a,Nb_bits_a)*pow2(Nb_bits_a); % nb sur 16 bits Q(16,0,15)

%coeff b
b = -2*a + 5/2;
Nb_bits_b = 15 - NbBits(b);
b_entier = FixCode(b,Nb_bits_b)*pow2(Nb_bits_b); % nb sur 16 bits Q(16,0,15)

%coeff c
c = a - 3/2;
Nb_bits_c = 15 - NbBits(c);
c_entier = FixCode(c,Nb_bits_c)*pow2(Nb_bits_c); % nb sur 16 bits Q(16,-3,18)

x_barre = x;     %(2/pi) * x;
Nb_bits_x_barre = 15 ;
x_barre_entier = FixCode(x_barre,Nb_bits_x_barre)*pow2(Nb_bits_x_barre); % nb sur 16 bits Q(16,0,15)

%% x_barre²
x_barre_carre_entier32 = mul_int16_int16(x_barre_entier,x_barre_entier); %sortie mult Q(32,1,30)
x_barre_carre_entier = rshift(x_barre_carre_entier32, 15); % nb sur 16 bits Q(16,0,15)

%% x_barre³
x_barre_cube_entier32 =  mul_int16_int16(x_barre_entier,x_barre_carre_entier); %sortie mult Q(32,1,30)
x_barre_cube_entier = rshift(x_barre_cube_entier32, 15); % nb sur 16 bits Q(16,0,15)

%% x_barre⁵
x_barre_cinq_entier32 =  mul_int16_int16(x_barre_cube_entier,x_barre_carre_entier); %sortie mult Q(32,1,30)
x_barre_cinq_entier = rshift(x_barre_cinq_entier32, 15); % nb sur 16 bits Q(16,0,15)

%--------------------------------------------------------------------------
%% a*x_barre

a_x_barre_entier32 = mul_int16_int16(a_entier,x_barre_entier); %sortie mult Q(32,2,29)

%% b*x_barre³

b_x_barre_cube_entier32 = mul_int16_int16(b_entier,x_barre_cube_entier); %sortie mult Q(32,1,30)
b_x_barre_cube_entier32 = rshift(b_x_barre_cube_entier32,1); %sortie  Q(32,2,29)

%% c*x_barre⁵
c_x_barre_cinq_entier32 = mul_int16_int16(c_entier,x_barre_cinq_entier); %sortie mult Q(32,-2,33)
c_x_barre_cinq_entier32 = rshift(c_x_barre_cinq_entier32,4); %sortie  Q(32,2,29)

somme_tempo = add_int32_int32(a_x_barre_entier32,b_x_barre_cube_entier32);
result = add_int32_int32(somme_tempo,c_x_barre_cinq_entier32);
result = rshift(result,14);
result = result*pow2(-15);

if negatif 
    result = -result;
end

end 
